import matplotlib.pyplot as plt
import pandas as pd
import statistics
# pwn
from pyanalysis.basic import extractsenan


def histfrequency(
        se_data, r_ymax=1, i_bin=16,
        s_facecolor="cyan", s_edgecolor="blue", i_edgewith=1,
        s_ylabel="frequency", s_xlabel="bins", s_title=None,
        b_sigmagrid=True, b_mean=True, b_median=True):
    '''
    frequency bin based histogram plot
    alternative for se_data.plot(kind="hist", bins=i_bin, color=s_color, grid=True, title=s_title)
    '''
    b_0k = False
    # data manipulation
    se_cut = pd.cut(se_data, bins=i_bin, labels=(range(i_bin)))
    se_frequency = pd.value_counts(se_cut) / len(se_cut)
    se_frequency = se_frequency.sort_index()
    # put figure
    o_fig = plt.figure()
    ax = o_fig.add_subplot(111)
    ax.set_ylim(0, r_ymax)
    ax.set_xlim(0, i_bin)
    # put plot
    ax.bar(height=se_frequency, left=range(i_bin), color=s_facecolor, edgecolor=s_edgecolor, linewidth=i_edgewith)
    # mean median
    if (b_median):
        r_xmedian = statistics.median(se_cut) + 0.5
    if (b_mean):
        r_xmean = statistics.mean(se_cut) + 0.5
    ax.annotate("median", xy=(r_xmedian, (r_ymax/10)*8), xytext=(r_xmedian, (r_ymax/10)*8.7), arrowprops=dict(arrowstyle="->"))
    ax.annotate("mean", xy=(r_xmean, (r_ymax/10)*7), xytext=(r_xmean, (r_ymax/10)*7.7), arrowprops=dict(arrowstyle="->"))
    # grid
    if (b_sigmagrid):
        i_2sigmabin = i_bin/6
        for i_mark in [2,3,4]:
            i_xaxis = i_2sigmabin * i_mark
            plt.plot([i_xaxis,i_xaxis],[0,r_ymax], linestyle="dashdot", color="black")
    else:
        ax.grid(True)
    # put lable
    ax.set_ylabel(s_ylabel)
    ax.set_xlabel(s_xlabel)
    ax.set_title(s_title)
    # out
    b_0k = True
    return(b_0k)


def scattercolor(df_data, s_y, s_x, ls_groupby=None, s_color=None, lf_yruler=None, lf_xruler=None, s_ylabel=None, s_xlabel=None, s_title=None):
    '''
    multi color scatter plot, colored by  groups
    '''
    b_0k= False
    # handle ls_groupby input
    dfgb_date = df_data.groupby(ls_groupby)
    ddf_data = dict(list(dfgb_date))
    lt_key = list(ddf_data.keys())
    # put figure
    o_fig = plt.figure()
    ax = o_fig.add_subplot(111)
    # for each grouped dataframe
    for t_key in lt_key:
        #print(t_key)
        df_thescatter = ddf_data[t_key]
        # handle s_color input
        s_thecolor = "cyan"
        if not (s_color is None):
            s_thecolor = extractsenan(df_thescatter[s_color])
        # put plot
        #df_thescatter.plot(kind="scatter", y=s_y, x=s_x, color=s_thecolor)
        plt.scatter(y=df_thescatter[s_y], x=df_thescatter[s_x], c=s_thecolor)
    # y and x ruler
    if (not (lf_yruler is None)) or (not (lf_xruler is None)):
        if (lf_yruler is None):
            lf_yruler = [min(df_data[s_y].values), max(df_data[s_y].values)]
        if (lf_xruler is None):
            lf_xruler = [min(df_data[s_x].values), max(df_data[s_x].values)]
        # put plot
        plt.plot(lf_xruler, lf_yruler, linestyle="dashdot", color="black")
    # put lable
    # y axis
    if not (s_ylabel is None):
        ax.set_ylabel(s_ylabel)
    else:
        ax.set_ylabel(s_y)
    # x axis
    if not (s_xlabel is None):
        ax.set_xlabel(s_xlabel)
    else:
        ax.set_xlabel(s_x)
    # title
    if not (s_title is None):
        ax.set_title(s_title)
    # out
    b_0k = True
    return(b_0k)
