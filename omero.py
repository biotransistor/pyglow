# python
import re
# scipy
import numpy as np
import pandas as pd
# matplotlib
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
# omeroidr: https://github.com/zegami/omero-idr-fetch
from omeroidr import connect
from omeroidr.images import Images


# example connect to omero and disconect
"""
from omeroidr import connect
s_url = "https://omero.ohsu.edu"
o_session = connect.connect_to_omero(s_url, user="", password="")
connect.disconnect(o_session, s_url)
"""

# example download image, image splited by channel, image thumbnales from omero
"""
from omeroidr.images import Images
s_url = "https://omero.ohsu.edu"
s_path ="./pixie/"
o_image = Images(o_session, s_url, s_path)
o_image.download_image(318764)
o_image.download_imagechannel(318764)
o_image.download_imagethumb(318764)
"""

# example get image metda data in json format from omero
"""
from omeroidr.data import Data
s_url = "https://omero.ohsu.edu"
o_data = Data(o_session, s_url)
d_image = o_data.get_imagedata(318764)
"""

# get x_object_inimage_px and y_object_inimage_px
def yxpixle(o_data, df_run, s_y="y_object_inimage", s_x="x_object_inimage", s_imageid="imageid"):
    """
    input:
        o_data: omeroidr.data object
        df_run: pandas dataframe with s_y, s_x and s_imageid column
        s_y: string which specifies object in image y coordinat column
        s_x: string which specifies object in image x coordinat column
        s_imageid: string which specifies image_id column

    output:
        df_run: pandas dataframe with additional
        x_object_inimage_px and y_object_inimage_px column.

    description:
        retrive object in image yx coordinat in pixle unit
    """
    df_out = df_run.copy()
    # get y and x pixle size assuming all df_run images have the same xy sizing
    d_json = o_data.get_imagedata(df_out[s_imageid][0])
    f_ydivisor = d_json["pixel_size"]["y"]
    f_xdivisor = d_json["pixel_size"]["x"]
    # calculate y x pixle coordinate
    df_out["y_object_inimage_px"] = np.round(df_out[s_y] / f_ydivisor)
    df_out["x_object_inimage_px"] = np.round(df_out[s_x] / f_xdivisor)
    # output
    return(df_out)


# get images form omero
def image_binmark_trait(
        df_data, s_signal="intensity_log2", ls_groupby=None, t_key=None,
        i_bin=72, li_binmark=None,
        s_iximageid="imageid", s_ixz=None, s_ixt=None, s_ixycoordinate="y_object_inimage_px", s_ixxcoordinate="x_object_inimage_px",
        s_ixrendertrait="render_stain", s_ixrendertraitset="render_stainset", s_path="./image/", s_url="https://omero.ohsu.edu", d_port={}):
    """
    """
    b_0k= False
    # handle input
    if (s_ixz == None):
        i_z = 0
    if (s_ixt == None):
        i_t = 0
    # handle ls_groupby input
    dfgb_data = df_data.groupby(ls_groupby)
    ddf_data = dict(list(dfgb_data))
    lt_key = list(ddf_data.keys())
    lt_key.sort()
    # for each grouped dataframe
    #print("Lost:", t_key)
    for t_index in lt_key:
        #print("Scan:", t_index)
        if (t_index == t_key):
            #print("& Found!")
            df_trait = ddf_data[t_key]
            # get binning
            se_trait = df_trait[s_signal]
            se_cut = pd.cut(se_trait, bins=i_bin, labels=range(i_bin))
            se_cut.name = "bin"
            df_fusion = pd.merge(df_trait, pd.DataFrame(se_cut), left_index=True, right_index=True)
            # set variable
            i_theimageid = None
            # get data
            df_fusion = df_fusion.reset_index()
            df_sorted = df_fusion.sort_values([s_iximageid, s_ixycoordinate, s_ixxcoordinate])
            # for each data row
            for o_index in df_sorted.index:
                df_row = df_sorted.ix[[o_index],[s_iximageid, s_ixycoordinate, s_ixxcoordinate, s_ixrendertrait, s_ixrendertraitset, "bin"]]
                a_row = df_row.values
                l_row = list(a_row[0])
                try:
                    i_imageid = int(l_row[0])
                    i_ycoordinate = int(l_row[1])
                    i_xcoordinate = int(l_row[2])
                    s_rendertrait = str(l_row[3])
                    s_rendertraitset = str(l_row[4])
                    i_thebin = int(l_row[5])
                    #print("Handle:", i_imageid, i_thebin, i_ycoordinate, i_xcoordinate, s_rendertrait, s_rendertraitset)
                    if (i_imageid != i_theimageid):
                        # start to omero session
                        o_session = connect.connect_to_omero(s_url, user=d_port[s_url][0], password=d_port[s_url][1])
                        # pull image
                        o_image = Images(o_session, s_url, s_path)
                        # stop omero session
                        connect.disconnect(o_session, s_url)
                        # load images
                        o_fig, o_axes = plt.subplots(2, 2, sharex=True, sharey=True, figsize=(22,16))
                        # Total channel image
                        o_axes[0,0].set_title("Image: {}\n{}".format(i_imageid, t_key))
                        o_image.download_image(i_imageid, render_setting=s_rendertraitset)
                        img = mpimg.imread(s_path+str(i_imageid)+"_z" + str(i_z) +"_t" + str(i_t) + s_rendertraitset + ".jpg")
                        o_axes[0,0].imshow(img)
                        # false color image
                        s_renderred = re.sub(r'\$[A-F0-9]+', '$FF0000', s_rendertrait)
                        o_image.download_image(i_imageid, render_setting=s_renderred)
                        img = mpimg.imread(s_path+str(i_imageid)+"_z" + str(i_z) +"_t" + str(i_t) + s_renderred + ".jpg")
                        lum_img = img[:,:,0]
                        o_imagefc = o_axes[0,1].imshow(lum_img, cmap="viridis")
                        plt.colorbar(o_imagefc, ax=o_axes[0,1], shrink=0.8)
                        # single channel image
                        o_image.download_image(i_imageid, render_setting=s_rendertrait)
                        img = mpimg.imread(s_path+str(i_imageid)+"_z" + str(i_z) +"_t" + str(i_t) + s_rendertrait + ".jpg")
                        o_axes[1,0].imshow(img)
                        # single channel image bin marked
                        o_image.download_image(i_imageid, render_setting=s_rendertrait)
                        img = mpimg.imread(s_path+str(i_imageid)+"_z" + str(i_z) +"_t" + str(i_t) + s_rendertrait + ".jpg")
                        o_axes[1,1].imshow(img)
                        # finalize
                        plt.subplots_adjust(wspace=0.0001, hspace=0.0001)
                        # development version
                        #if (i_theimageid != None):
                        #    break
                        # set variable
                        i_theimageid = i_imageid
                    # mark object in image
                    if (li_binmark is None) or (i_thebin in li_binmark):
                        o_axes[1,1].annotate(i_thebin, xy=(i_xcoordinate, i_ycoordinate), color="Chocolate") # fontsize=9
                    else:
                        o_axes[1,1].annotate("+", xy=(i_xcoordinate, i_ycoordinate), color="Chocolate") # fontsize=9
                    # development
                    #break
                except ValueError:
                    #print("NOP:", l_row)
                    pass
            # out
            break


#def image_binmark_traitset()
