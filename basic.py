"""
title: basic.py
date: 2016-08-18
author: bue
language: python 3.5
description:
  basic routines used in everiday data analysis programming
"""
# standard library
import copy
import math
import numpy as np
import pandas as pd
import re

# function
def retype(o_value):
    """
    description:
      original from annot/web/prjannot/structure.py
      mainly to transform string to type
      bue 20160927: this sound somehow similar to about the same as stack a dataframe
    """
    if not((type(o_value) == bool) or (o_value == None)):
        # if not a number
        if not((type(o_value) == float) or (type(o_value) == int) or (type(o_value) == complex)):
            # strip
            o_value = str(o_value)
            o_value = o_value.strip()
            # number
            if re.fullmatch("[-|+|0-9|.|e|E|j|(|)]+", o_value):
                # complex
                if re.search("j", o_value):
                    try:
                        o_value = complex(o_value)
                    except ValueError:
                        pass  # still a string
                # float
                elif re.search("[.|e|E]", o_value):
                    try:
                        o_value = float(o_value)
                    except ValueError:
                        pass  # still a string
                # integer
                else:
                    try:
                        o_value = int(o_value)
                    except ValueError:
                            pass  # still a string
            # boolean
            elif (o_value in ('TRUE','True','true')):
                o_value = True
            elif (o_value in ('FALSE','False','false')):
                o_value = False
            elif (o_value in ('NONE','None','none','Null','Null','null')):
                o_value = None
            # a real string, complex, float or integer
            else:
                pass
    # output
    return(o_value)


def extractsenan(se_data):
    '''
    # subtype cellline
    '''
    lo_extract = list(se_data.unique())
    try:
        lo_extract.pop(lo_extract.index(np.NAN))
    except ValueError:
        pass
    o_extract = lo_extract[0]
    return(o_extract)


def tuplestring2lo(s_tuple):
    """
    turn tuple string into
    a list of objects
    """
    s_tuple = s_tuple.replace("(","")
    s_tuple = s_tuple.replace(")","")
    ls_tuple = s_tuple.split(",")
    lo_tuple = [retype(s) for s in ls_tuple]
    return(lo_tuple)


def sampler(df_data, ls_groupby, r_sample=0.333, i_seed=None):
    # set output variable
    df_sample = None
    # handle input
    dfgb_date = df_data.groupby(ls_groupby)
    ddf_data = dict(list(dfgb_date))
    lt_key = list(ddf_data.keys())
    # sample
    for t_key in lt_key:
        df_data = ddf_data[t_key]
        l_index = list(df_data.index)
        i_total = len(l_index)
        i_sample = math.ceil(i_total*r_sample)
        np.random.seed(i_seed)
        li_total = np.random.permutation(l_index)
        li_sample = li_total[:i_sample]
        df_take = df_data.loc[li_sample,:]
        if (df_sample is None):
            df_sample = copy.deepcopy(df_take)
        else:
            df_sample = pd.concat([df_sample,df_take])
    # output
    return(df_sample)
