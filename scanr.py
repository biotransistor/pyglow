"""
titlr: scanr.py
date: 2016-08-18
author: bue
language: python 3.5
description:
  code to fuse experimental blueprint and single cell scanr output via pandas
"""
# standard library
import csv
import matplotlib.pyplot as plt
import pandas as pd
import sys

# own library
from pyanalysis.basic import retype

# functions
def read_datscanr(s_pathfile):
    """
    description:
      read scanr dat files (experiment_descriptor.dat, acquisitionlog.dat)
      returns pandas df
    """
    # initialize file handle
    with open(s_pathfile) as f:
        o_reader = csv.reader(f, delimiter='\t')
        # read file
        s_name = None
        d_data = {}
        for ls_record in o_reader:
            # jump over cmd rows
            if (len(ls_record) > 2):
                # get df name
                s_pop = ls_record.pop(0)
                if (s_name == None):
                    s_name = s_pop
                else:
                    if (s_name != s_pop):
                        sys.exit(
                            "Error read_datscanr: strange scanr dat file. \
first column of data row is not constantely the same: {}, {}. \
can not generate dataframe name".format(s_name, s_pop))
                # get df record
                for s_entry in ls_record:
                    s_serie, s_value = s_entry.split("=")
                    o_value = retype(s_value)
                    try:
                        lo_value = d_data[s_serie]
                    except KeyError:
                        lo_value = []
                    lo_value.append(o_value)
                    d_data.update({s_serie:lo_value})
    # transform dictionary to pandas dataframe
    df_datscanr = pd.DataFrame(d_data)
    # output
    return(df_datscanr)


def scanr2df(s_run, s_tsvcellline, s_tsvstain, s_datexperimentdescriptor, s_datacquisitionlog, s_tsvmain, s_tsvcyto):
    """
    description:
      fuse run infromaton form experimental blueprint and scanar output files
      into a pandas dataframe
    """
    # channel constants
    s_wave0 = "nm395_"
    s_wave1 = "nm488_"
    s_wave2 = "nm555_"
    s_wave3 = "nm647_"

    # wetlab scientist blue print
    df_cellline = pd.read_csv(s_tsvcellline, sep='\t', header=0, index_col=0)
    se_cellline = df_cellline.stack()
    se_cellline.index = ["".join(ts_index) for ts_index in se_cellline.index.tolist()]
    se_cellline.name = "cellline"
    df_stain = pd.read_csv(s_tsvstain, sep='\t', header=0, index_col=0)
    se_stain = df_stain.stack()
    se_stain.index = ["".join(ts_index) for ts_index in se_stain.index.tolist()]
    se_stain.name = "stainset"
    df_blueprint = pd.DataFrame(se_cellline).join(se_stain)
    # handle stains
    ls_wave0mean = []
    ls_wave0total = []
    ls_wave1mean = []
    ls_wave2mean = []
    ls_wave3mean = []
    for s_entry in df_blueprint.stainset:
        ls_entry = s_entry.split(";")
        ls_entry = [s_entry.strip() for s_entry in ls_entry]
        ls_wave0mean.append(s_wave0 + ls_entry[0] + "_mean")
        ls_wave0total.append(s_wave0 + ls_entry[0] + "_total")
        ls_wave1mean.append(s_wave1 + ls_entry[1] + "_mean")
        ls_wave2mean.append(s_wave2 + ls_entry[2] + "_mean")
        ls_wave3mean.append(s_wave3 + ls_entry[3] + "_mean")
    # join stains
    se_wave0mean = pd.Series(ls_wave0mean, index=df_blueprint.index)
    se_wave0mean.name = s_wave0+"label_mean"
    se_wave0total = pd.Series(ls_wave0total, index=df_blueprint.index)
    se_wave0total.name = s_wave0+"label_total"
    se_wave1mean = pd.Series(ls_wave1mean, index=df_blueprint.index)
    se_wave1mean.name = s_wave1+"label_mean"
    se_wave2mean = pd.Series(ls_wave2mean, index=df_blueprint.index)
    se_wave2mean.name = s_wave2+"label_mean"
    se_wave3mean = pd.Series(ls_wave3mean, index=df_blueprint.index)
    se_wave3mean.name = s_wave3+"label_mean"
    df_blueprint = pd.DataFrame(df_blueprint).join([se_wave0mean, se_wave0total, se_wave1mean, se_wave2mean, se_wave3mean])

    # scanr dat
    df_experimentdescriptor = read_datscanr(s_datexperimentdescriptor)
    df_acquisitionlog = read_datscanr(s_datacquisitionlog)
    df_datscanr = pd.merge(df_experimentdescriptor, df_acquisitionlog, left_on=["WELL","SUBPOS"], right_on=["W","P"])
    if (len(df_datscanr) != len(df_acquisitionlog)) or (len(df_datscanr) != len(df_experimentdescriptor)):
        sys.exit("Error main: scanr dat files can not be merged. df_datscanr length: {}, df_experimentdescriptor length: {}, df_acquisitionlog length: {}.".format(len(df_datscanr), len(df_experimentdescriptor), len(df_acquisitionlog)))
    # drop redundant series
    df_datscanr01 = df_datscanr.drop(["TIME_x","W","P","X","Y","Z"], axis=1)  # bue 20160818: no idea what AF and T is
    df_datscanr01["PLATE"] = s_run

    # scanr single cell data main
    # bue 20160928: from any main stain total intensity can be calculated
    df_main = pd.read_csv(s_tsvmain, sep='\t', header=0) # index_col='Object ID'
    df_main.columns = [s_column.strip() for s_column in df_main.columns]
    df_main[s_wave0 + "total"]= df_main["Mean Intensity DAPI"] * df_main["Area"] # calculare dapi total intensity form dapi mean intensity and area
    # scanr single cell data cyto
    # bue 20160928: cyto stain is doughnut based, only mean intensity makes sense.
    df_cyto = pd.read_csv(s_tsvcyto, sep='\t', header=0) #  index_col='Object ID'
    df_cyto.columns = [s_column.strip() for s_column in df_cyto.columns]
    df_singlecellscanr = pd.merge(df_main, df_cyto, on="Object ID") # merge on Object_ID which is index, alterfative: df_cellscanr = df_main.join(df_cyto)

    # fuse
    df_run = pd.merge(df_blueprint, df_datscanr01, left_index=True, right_on="ID")
    df_run = pd.merge(df_run, df_singlecellscanr, left_on=["WELL","SUBPOS"], right_on=["Well","Position"])
    # image_inwell calculation
    df_run["image_inwell"] = df_run["SUBPOS"] -1
    # coordinate calculation
    df_run["x_object_inimage"] = df_run["IMAGEX"] + df_run["X"]
    df_run["x_object_inplate"] = df_run["IMAGEX"] + df_run["x_object_inimage"]
    df_run["y_object_inimage"] = df_run["IMAGEY"] - df_run["Y"]
    df_run["y_object_inplate"] = df_run["IMAGEY"] - df_run["y_object_inimage"]
    # drop series
    df_run = df_run.drop(["AF","SUBPOS","T","Well","Position","X","Y","Parent Object ID (Well)","Parent Trace ID_x","Area_y","Parent Object ID (MO)","Parent Trace ID_y"], axis=1)

    # rename
    ls_column = list(df_run.columns)
    ls_column[ls_column.index("PLATE")] = "plate"
    ls_column[ls_column.index("WELL")] = "well_i"
    ls_column[ls_column.index("ID")] = "well_si"
    ls_column[ls_column.index("IMAGEX")] = "x_image_inplate"
    ls_column[ls_column.index("IMAGEY")] = "y_image_inplate"
    ls_column[ls_column.index("IMAGEZ")] = "z_image"
    ls_column[ls_column.index("TIME_y")] = "time_image"
    ls_column[ls_column.index("Object ID")] = "id_object"
    ls_column[ls_column.index("Mean Intensity DAPI")] = s_wave0 +"mean"
    ls_column[ls_column.index("Area_x")] = s_wave0 + "area"
    ls_column[ls_column.index("Mean Intensity Alexa 488")] = s_wave1 +"mean"
    ls_column[ls_column.index("Mean Intensity Alexa 555")] = s_wave2 +"mean"
    ls_column[ls_column.index("Mean Intensity Alexa 647")] = s_wave3 +"mean"
    df_run.columns = ls_column

    # sort columns via reindexing
    ls_index = [
        "plate","well_i","well_si","image_inwell","id_object",
        "x_object_inplate","y_object_inplate",
        "x_image_inplate","y_image_inplate",
        "x_object_inimage","y_object_inimage",
        "z_image","time_image",
        "cellline",
        "stainset",
        "nm395_label_mean","nm395_mean","nm395_area","nm395_label_total","nm395_total",
        "nm488_label_mean","nm488_mean",
        "nm555_label_mean","nm555_mean",
        "nm647_label_mean","nm647_mean"]
    df_run01 = df_run.reindex(columns=ls_index)

    # output
    return(df_run01)
