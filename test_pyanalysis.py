import unittest

import numpy as np
import pandas as pd

# own library
from pyanalysis import basic, omero, plot, scanr

# test routines
class TestPyanalysisBasic(unittest.TestCase):
    """
    basic methods unittest
    """
    def test_sampler(self):
        # result
        ei_result = {0,4,5,6,7,8}
        # data
        df_data = pd.DataFrame(
            {"key1" : ["a","a","b","b","a","a","a","b","b","a","a","a","b","b","a","a"],
             "key2" : ["one","two","one","two","one","one","two","one","two","one","one","two","one","two","one","one"],
             "data1" : np.random.randn(16),
             "data2" : np.random.randn(16)})
        # manipu
        df_sampler = basic.sampler(df_data, ls_groupby=["key1","key2"], r_sample=0.333, i_seed=41111413)
        ei_index =  set(df_sampler.index)
        # assert
        self.assertEqual(ei_result, ei_index)

#class TestPyanalysisOmero(unittest.TestCase):
#    """
#    omero methods unittest
#    """

class TestPyanalysisPlot(unittest.TestCase):
    """
    plot methods unittest
    """
    def test_histfrequency(self):
        b_0k = False
        # data
        se_data = pd.Series(np.random.randn(512))
        # plot
        b_0k = plot.histfrequency(
            se_data, r_ymax=0.2, i_bin=16,
            s_facecolor="cyan", s_edgecolor="blue", i_edgewith=1,
            s_ylabel="frequency", s_xlabel="bins", s_title=None,
            b_sigmagrid=True, b_mean=True, b_median=True)
        # assert
        self.assertTrue(b_0k)

    def test_scattercolor(self):
        b_0k = False
        # data
        df_data = pd.DataFrame(
            {"key1" : ["a","a","b","b","a","a","a","b","b","a","a","a","b","b","a","a"],
             "key2" : ["one","two","one","two","one","one","two","one","two","one","one","two","one","two","one","one"],
             "farbe1": ["maroon","maroon","red","red","maroon","maroon","maroon","red","red","maroon","maroon","maroon","red","red","maroon","maroon"],
             "farbe2" : ["cyan","blue","cyan","blue","cyan","cyan","blue","cyan","blue","cyan","cyan","blue","cyan","blue","cyan","cyan"],
             "data1" : np.random.randn(16),
             "data2" : np.random.randn(16)})
        # plot
        b_0k = plot.scattercolor(df_data=df_data, s_y="data1", s_x="data2", ls_groupby=["key1","key2"], s_color="farbe1")
        # assert
        self.assertTrue(b_0k)


#class TestPyanalysisBasic(unittest.TestCase):
#    """
#    scanr methods unittest
#    """
#    pass


if __name__ == '__main__':
    unittest.main(warnings='ignore')
